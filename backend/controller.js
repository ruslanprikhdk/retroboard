const User = require('./models/User');
const Column = require('./models/Column');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');

class Controller {

  async getData (req, res) {
    try {    
      const column = await Column.find();
      return res.status(200).send(column);
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }  

  async addNewColumn (req, res) {
    try {    
      const column = new Column(req.body);
      await column.save();
      return res.status(200).send({message:'Column added successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }

  async modifyColumn (req, res) {
    try {
      const column = new Column(req.body);
      const id = String(column.id);
      const list = column.list;
      let columnToModify = await Column.findOne({id});
      columnToModify.list = list;
      await columnToModify.save();
      return res.status(200).send({message:'Column modified successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async modifyColumnColor (req, res) {
    try {
      const column = new Column(req.body);
      const id = String(column.id);
      const color = column.color;
      let columnToModify = await Column.findOne({id});
      columnToModify.color = color;
      await columnToModify.save();
      return res.status(200).send({message:'Column modified successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async dropDtbUpdate (req, res) {
    try {
      const {previousColumnNumber, currentColumnNumber, card, previousIndex, currentIndex} = req.body;
      const columns = await Column.find();
      const previousColumn = columns[previousColumnNumber];
      previousColumn.list.splice(previousIndex, 1);
      await previousColumn.save();
      const currentColumn = columns[currentColumnNumber];
      currentColumn.list.splice(currentIndex, 0, card);
      await currentColumn.save();
      return res.status(200).send({message:'Column updated successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async dropColumnUpdate (req, res) {
    try {
      const {previousColumnNumber, previousIndex, currentIndex} = req.body;
      const columns = await Column.find();
      const targetColumn = columns[previousColumnNumber];
      let targetColumnCard = targetColumn.list[previousIndex];
      targetColumn.list.splice(previousIndex, 1);
      targetColumn.list.splice(currentIndex, 0, targetColumnCard);
      await targetColumn.save();
      return res.status(200).send({message:'Column updated successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async deleteColumn (req, res) {
    try {
      const id = req.path.substring(15);
      const column = await Column.findOne({id});
      column.remove();
      return res.status(200).send({message:'Column deleted successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async getUserName (req, res) {
    try {
      const {userEmail} = req.body;
      return res.status(200).send({message:'Name received successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }    
  }

  async registration (req, res) {
    try {
      const errors = validationResult(req);
      if(!errors.isEmpty()) {
        return res.status(400).send({message:'Input is not valid'});
      }
      const {name, email, password} = req.body;
      const candidate = await User.findOne({email});
        if (candidate) {
          return res.status(400).send({message:'User already exists'});
        } 
      const createdDate = new Date();
      const protectedPassword = await bcrypt.hash(password, 10);
      const user = new User({name, email, password: protectedPassword, createdDate: createdDate.toISOString()});
      await user.save();
      return res.status(200).send({message:'Profile created successfully'});

    } catch {
      res.status(400).send({message:'Please, check your input'});
    }
  }

  async login (req, res) {
    try {
      const {email, password} = req.body;
      const user = await User.findOne({email});
      if(!user) {
        return res.status(400).send({message: `User with email ${email} is not found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if(!validPassword) {
        return res.status(400).send({message: 'Please, check your password'});
      }
      return res.status(200).send({userName: user.name});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }
  }
}

module.exports = new Controller();

