const {Schema, model} = require('mongoose');

const Column = new Schema({ 
  title: {type: String, required: true},
  color: {type: String, required: true},
  list: {type: Array, required: true},
  id: {type: Number, required: true}
})

module.exports = model('Column', Column);