const Router = require('express');
const router = new Router();
const controller = require('../controller');
const {check} = require('express-validator');

router.get('/get_data', controller.getData);
router.post('/get_user_name', controller.getUserName);
router.put('/modify_column', controller.modifyColumn);
router.put('/modify_column_color', controller.modifyColumnColor);
router.put('/drop_update', controller.dropDtbUpdate);
router.put('/drop_column_update', controller.dropColumnUpdate);
router.delete('/delete_column/:id', controller.deleteColumn);
router.post('/add_column', controller.addNewColumn);
router.post('/auth/register', [
  check('email', 'Incorrect email').isEmail()
], controller.registration);
router.post('/auth/login', controller.login);

module.exports = router;