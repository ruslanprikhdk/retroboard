import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Retroboard';

  constructor(private router: Router) { }

  ngOnInit(): void {
    let userEmail = localStorage.getItem('boardUserEmail');
    if (!userEmail) {
      return;
    }
    this.router.navigate(['board']);
  }
}
