import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddItemWindowComponent } from './add-item-window/add-item-window.component';
import { AddItemTextComponent } from './add-item-text/add-item-text.component';

import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    AddItemWindowComponent,
    AddItemTextComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule
  ],
  exports: [
    AddItemWindowComponent
  ]
})
export class AddItemBtnModule { }
