import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddItemTextComponent } from './add-item-text.component';

describe('AddItemTextComponent', () => {
  let component: AddItemTextComponent;
  let fixture: ComponentFixture<AddItemTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddItemTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddItemTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
