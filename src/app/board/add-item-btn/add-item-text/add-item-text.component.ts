import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-item-text',
  templateUrl: './add-item-text.component.html',
  styleUrls: ['./add-item-text.component.css']
})
export class AddItemTextComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddItemTextComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
