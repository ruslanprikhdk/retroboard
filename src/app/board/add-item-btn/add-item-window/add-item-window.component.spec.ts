import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddItemWindowComponent } from './add-item-window.component';

describe('AddItemWindowComponent', () => {
  let component: AddItemWindowComponent;
  let fixture: ComponentFixture<AddItemWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddItemWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddItemWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
