import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddItemTextComponent } from '../add-item-text/add-item-text.component';

@Component({
  selector: 'app-add-item-window',
  templateUrl: './add-item-window.component.html',
  styleUrls: ['./add-item-window.component.css']
})
export class AddItemWindowComponent implements OnInit {
  
  @Input() question:any;
  
  @Output() emitText: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddItemTextComponent, {
      width: '400px',
      data: {question: this.question}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.emitText.emit(result);
    });

  }
}
