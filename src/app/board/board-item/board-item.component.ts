import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-board-item',
  templateUrl: './board-item.component.html',
  styleUrls: ['./board-item.component.css']
})
export class BoardItemComponent implements OnInit {

  @Input() item:any;

  @Output() emitText:EventEmitter<{id:number; userName:any, text:string}> = new EventEmitter();

  @Output() emitCardItem: EventEmitter<{card:any; increase:boolean}> = new EventEmitter();

  @Output() emitDeleteCard: EventEmitter<number> = new EventEmitter();

  commentInput = '';
  open = false;

  constructor() { }

  ngOnInit(): void {
  }

  userNameUpd = this.getUserName();

  getUserName() {
    let userName:any = localStorage.getItem('boardUserName');
    let userNameUpd = JSON.parse(userName);
    return userNameUpd.userName;
  } 

  onOpenComment() {
    this.open = !this.open;
  }

  onCommentTextEmit(id:number) {
    this.emitText.emit({id, text:this.commentInput, userName:this.userNameUpd});
    this.commentInput = '';
  }
 
  onCardItemEmit(card:any, increase:boolean) {
    this.emitCardItem.emit({card, increase});
  }

  onCardDelete(id:number) {
    this.emitDeleteCard.emit(id);
  }

}
