import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { BoardService } from '../service/board.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit { 

  constructor(
    public boardService: BoardService, private router: Router
  ) { }

  ngOnInit(): void {
    let userEmail = localStorage.getItem('boardUserEmail');
    if (!userEmail) {
      this.router.navigate(['login']);
    }    
  }

  onColorChange(color:string, columnId:number) {
    this.boardService.changeColumnColor(color, columnId);
  }

  onAddCard(text:string, columnId:number) {
    if(text) {
      this.boardService.addCard(text, columnId);
    }
  }

  onDeleteColumn(columnId:number) {
    this.boardService.deleteColumn(columnId);
    this.boardService.deleteColumnInDtb(columnId).subscribe(); 
  }

  onDeleteCard(cardId:number, columnId:number) {
    this.boardService.deleteCard(cardId, columnId);
  }

  onChangeLike(event: {card:any, increase:boolean}, columnId:number) {
    const {card: {id}, increase} = event;
    this.boardService.changeLike(id, columnId, increase);
  }

  onAddComment(event:{id:number, text:string, userName:string}, columnId:number) {
    this.boardService.addComment(columnId, event.id, event.text, event.userName);
  }

  onDeleteComment(comment:any, columnId:any, item:any) {
    this.boardService.deleteComment(columnId, item.id, comment.id);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      this.boardService.dropColumnUpdate(String(event.previousContainer.id).split("-")[3], event.previousIndex, event.currentIndex).subscribe();
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,       
      );
      this.boardService.dropDtbUpdate(String(event.previousContainer.id).split("-")[3], String(event.container.id).split("-")[3], event.container.data[event.currentIndex], event.previousIndex, event.currentIndex).subscribe();
    }
  }
}
