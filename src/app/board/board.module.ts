import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { FormsModule } from '@angular/forms';
import { HeaderModule } from '../header/header.module';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { BoardItemComponent } from './board-item/board-item.component';
import { CommentItemComponent } from './comment-item/comment-item.component';
import { AddItemBtnModule } from './add-item-btn/add-item-btn.module';
import { ColorPanelComponent } from './color-panel/color-panel.component';

@NgModule({
  declarations: [
    BoardComponent,
    BoardItemComponent,
    CommentItemComponent,
    ColorPanelComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    DragDropModule,
    MatButtonModule,
    FormsModule,
    AddItemBtnModule,
    HeaderModule
  ],
  exports: [
    BoardComponent
  ]
})
export class BoardModule { }
