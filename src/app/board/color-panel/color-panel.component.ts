import { Component, OnInit, Output, EventEmitter } from '@angular/core';

enum colors {
  LIGHTGRAY = "#d3d3d3",
  LIGHTGREEN = "#90ee90",
  LIGHTBLUE = "#add8e6",
  LIGHTCORAL = "#f08080",
  KHAKI = "#f0e68c",
  LIGHTSALMON = "#ffa07a"
}

@Component({
  selector: 'app-color-panel',
  templateUrl: './color-panel.component.html',
  styleUrls: ['./color-panel.component.css']
})
export class ColorPanelComponent implements OnInit {

  @Output() emitColor: EventEmitter<string> = new EventEmitter(); 

  colorsData = Object.values(colors);

  constructor() { }

  ngOnInit(): void {
  }

  onColorEmit(color:string) {
    this.emitColor.emit(color);
  }

}
