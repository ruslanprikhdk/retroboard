import { Component, OnInit, Output, Input } from '@angular/core';
import { BoardService } from 'src/app/service/board.service';
import { HttpService } from 'src/app/service/http.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public boardService: BoardService, private httpService: HttpService, private router: Router
  ) {
    
   }

  ngOnInit(): void {
    
  }

  userName = this.getUserName();

  getUserName() {
    let userName:any = localStorage.getItem('boardUserName');
    let userNameUpd = JSON.parse(userName);
    return userNameUpd.userName;
  }

  addColumn(event:string) {
    if (event) {
      let id = Date.now();
      this.boardService.addColumn(event, id);
      this.boardService.addColumnToDtb({title:event,color:'#add8e6',list:[],id:id}).subscribe();
    }
  }

  logout() {
    localStorage.removeItem('boardUserEmail');
    localStorage.removeItem('boardUserName');
    this.router.navigate(['login']);

  }
  
  downloadData() {
    let prelimData = this.boardService.board$.value;
    let dataToExcel:any = [];
    prelimData.forEach(column => {
      let itemToExcel = {
        id: '',
        title: '',
        color: '',
        cards: ''
      };
      itemToExcel.id = column.id;
      itemToExcel.title = column.title;
      itemToExcel.color = column.color;
      if(column.list.length > 0) {
        let cardsList = '';
        for (let i = 0; i < column.list.length; i++) {
          if (i === 0) {
            cardsList = column.list[i].text;
          } else {
            cardsList = cardsList + ", " + column.list[i].text;
          }
          
        }
        itemToExcel.cards = cardsList;
      }
      dataToExcel.push(itemToExcel);
    })

    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Retro board',
      useBom: true,
      headers: ["Id", "Column title", "Column color", "Cards list"]
    };
   
    new ngxCsv(dataToExcel, "Report", options);
  }

}
