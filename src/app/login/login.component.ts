import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { UserAuth } from 'src/app/models/userAuth';
import { Router } from '@angular/router';
import { BoardService } from 'src/app/service/board.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userAuth = {
    email: '',
    password: ''
  }
  constructor(public boardService: BoardService, private httpService: HttpService, private router: Router) { }

  ngOnInit(): void {
  }

  loginError = false;

  login(user: UserAuth) {
    this.httpService.loginUser(user).subscribe(
      (data) => { 
        if (data) {
          localStorage.setItem('boardUserEmail', `${this.userAuth.email}`);
          localStorage.setItem('boardUserName', `${JSON.stringify(data)}`);         
          f();
          async function f(this:any) {
            await window.location.reload();
            this.redirectToBoard();
            this.userAuth.email = '';
            this.userAuth.password = '';
          }
        }
      },
      (error) => {
        this.loginError = true;
      });      
  }

  redirectToBoard() {
    this.router.navigate(['board']);
  }

  redirectToRegistration() {
    this.router.navigate(['registration']);
  }

}

