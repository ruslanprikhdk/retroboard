export interface ColumnToAdd {
  title:String;
  color:String;
  list:[];
  id:Number;
}