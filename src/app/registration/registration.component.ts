import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { UserToRegister } from 'src/app/models/userToRegister';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  userToRegister = {
    name: '',
    email: '',
    password: ''
    
  }
  constructor(private httpService: HttpService, private router: Router) { }

  ngOnInit(): void {
  }

  registrationError = false;

  add(user: UserToRegister) {
    this.httpService.addUser(user).subscribe(
      (data) => {
        if (data) {
        this.userToRegister.name = '';
        this.userToRegister.email = '';
        this.userToRegister.password = '';  
        this.router.navigate(['login']);
        }    
      },
      (error) => {
        this.registrationError = true;
      }                
    ); 
  }

  redirectToLogin() {
    this.router.navigate(['login']);
  }

}
