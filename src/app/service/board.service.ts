import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';

import { ColumnToAdd } from '../models/columnToAdd';

@Injectable({
  providedIn: 'root',
})

export class BoardService {

  private userUrl = 'http://localhost:8080/api/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) {
    this.getData();
   }

  private initBoard = [
    
  ];
 
  public board:any[] = this.initBoard;
  public board$ = new BehaviorSubject<any[]>(this.initBoard);

  addColumnToDtb(column: ColumnToAdd): Observable<ColumnToAdd> {
    return this.http.post<ColumnToAdd>(`${this.userUrl}add_column`, column, this.httpOptions);
  }

  modifyColumnInDtb(column: ColumnToAdd): Observable<ColumnToAdd> {
    return this.http.put<ColumnToAdd>(`${this.userUrl}modify_column`, column, this.httpOptions);
  }

  modifyColumnColorInDtb(column: ColumnToAdd): Observable<ColumnToAdd> {
    return this.http.put<ColumnToAdd>(`${this.userUrl}modify_column_color`, column, this.httpOptions);
  }

  dropDtbUpdate(previousColumnNumber:string, currentColumnNumber:string, card:any, previousIndex:number, currentIndex:number) {
    return this.http.put(`${this.userUrl}drop_update`, {previousColumnNumber, currentColumnNumber, card, previousIndex, currentIndex}, this.httpOptions);
  }

  dropColumnUpdate(previousColumnNumber:string, previousIndex:number, currentIndex:number) { 
    return this.http.put(`${this.userUrl}drop_column_update`, {previousColumnNumber, previousIndex, currentIndex}, this.httpOptions);
  }

  getData() {
    return this.http.get(`${this.userUrl}get_data`,{responseType:'text'})
      .subscribe(
        val => {
          this.board = JSON.parse(val);
          this.board$.next([...this.board]);
        }
      );
  }

  getBoard$() {
    return this.board$.asObservable();
  }

  getUserName(userEmail:any) {
    return this.http.post(`${this.userUrl}get_user_name`, userEmail, this.httpOptions).subscribe();
  }

  changeColumnColor(color:string, columnId:number) {
    this.board = this.board.map((column:any) => {
      if (column.id === columnId) {
        column.color = color;
        this.modifyColumnColorInDtb(column).subscribe();
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  addColumn(title:string, id:number) {
    const newColumn:any = {
      id: id,
      title: title,
      color: '#add8e6',
      list: []
    };
    this.board = [...this.board, newColumn];
    this.board$.next([...this.board]);
  }

  addCard(text:string, columnId:number) {

    let userName = localStorage.getItem('boardUserName');
    if(userName) {
      userName = JSON.parse(userName).userName;
    }
    const createdDate = new Date();
    const newCard:any = {
      id: Date.now(),
      text,
      likes: 0,
      comments: [],
      userName,
      createdDate: createdDate.toISOString()
    };

    this.board = this.board.map((column:any) => {
      if(column.id === columnId) {
        column.list = [newCard, ...column.list];
        this.modifyColumnInDtb(column).subscribe();
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  deleteColumn(columnId:any) {
    this.board = this.board.filter((column:any) => column.id !== columnId);
    this.board$.next([...this.board]);
  }

  deleteColumnInDtb(columnId:any) {
    return this.http.delete(`${this.userUrl}delete_column/${columnId}`, this.httpOptions);
  }

  deleteCard(cardId:number, columnId:number) {
    this.board = this.board.map((column:any) => {
      if (column.id === columnId) {
        column.list = column.list.filter((card:any) => card.id !== cardId);
        this.modifyColumnInDtb(column).subscribe();
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  changeLike(cardId:number, columnId:number, increase:boolean) {
    this.board = this.board.map((column:any) => {
      if (column.id === columnId) {
        const list = column.list.map((card:any) => {
          if(card.id === cardId) {
           
            if(increase) {
              card.likes++;
            } else {
              if(card.likes > 0) {
                card.likes--;
              }
            }
          }
          return card;
        });
        column.list = list;
        this.modifyColumnInDtb(column).subscribe();
        return column;
      } else {
        return column;
      }
    });
    this.board$.next([...this.board]);
  }

  addComment(columnId: number, cardId: number, text:string, userName: string) {
    this.board = this.board.map((column:any) => {
      if (column.id === columnId) {
        const list = column.list.map((card:any) => {
          if(card.id === cardId) {
            const createdDate = new Date();
            const newComment = {
              id: Date.now(),
              text,
              userName,
              createdDate: createdDate.toISOString()
            };
            card.comments = [newComment, ...card.comments];
          }
          return card;
        });
        column.list = list;
        this.modifyColumnInDtb(column).subscribe();
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  deleteComment(columnId:any, itemId:any, commentId:any) {
    this.board = this.board.map(column => {
      if (column.id === columnId) {
        const list = column.list.map((item:any) => {
          if(item.id === itemId) {
            item.comments = item.comments.filter((comment:any) => {
              return comment.id !== commentId;
            })
          }
          return item;
        })
        column.list = list;
        this.modifyColumnInDtb(column).subscribe();
      }
      return column;
    })
    this.board$.next([...this.board]);
  }
 
}
