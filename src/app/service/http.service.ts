import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserToRegister } from '../models/userToRegister';
import { UserAuth } from '../models/userAuth';

import { Observable } from 'rxjs';



@Injectable({ providedIn: 'root' })
export class HttpService {

  private userUrl = 'http://localhost:8080/api/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  addUser(user: UserToRegister): Observable<UserToRegister> {
    return this.http.post<UserToRegister>(`${this.userUrl}auth/register`, user, this.httpOptions);
  }

  loginUser(user: UserAuth): Observable<UserAuth> {
    return this.http.post<UserAuth>(`${this.userUrl}auth/login`, user, this.httpOptions);
  }

}